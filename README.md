# Lithica-tech
Documentation de l'aspect technique du projet Lithica.


## Branchements
Suivre les consignes de branchements documentées sur cette image : [wiring.png](./hardware/wiring.png)


## Utilisation
- Vérifier les branchements. Allumer la multiprise et l'ordinateur.
- **Ordi / Allumage**. À l'allumage, l'ordinateur ouvre automatiquement un patch Pure Data (Pd) situé à cette adresse "/home/pi/lithica-tech/puredata/laptop/main.pd". Si besoin d'ouvrir le patch manuellement, cliquer sur l'icône de notes de musique située en bas dans le tableau de bord.
- **Pd / Patch puredata/laptop/main.pd**. Le patch permet  :
	- de visualiser les données entrantes des capteurs qui proviennent de la Raspberry Pi (cliquer dans le carré blanc à côté de "SENSORS"). À noter qu'on peut utiliser la souris pour faire bouger les glissières au cas où il n'y a pas de capteurs branchés ;
	- de choisir un son et son volume ;
	- de choisir un rendu graphique. Ce n'est pas optimisé donc à utiliser avec précautions pour le moment.
- **Pd / Modes action et édition**. Pour utiliser le patch, il faut au moins savoir que dans Pd, il y a deux modes : le mode action dans lequel l'image du curseur est une flèche, qui permet de cliquer et d'interagir avec les objets graphiques ; et le mode édition où l'image du curseur est une main, qui permet d'écrire, de déplacer, supprimer, etc. Pour basculer d'un mode à l'autre : menu édition > décocher ou cocher "mode édition", ou alors le raccourci clavier "Ctrl-e".
- **Pd / Sélectionner les sons**. Pour sélectionner un son, il faut cliquer sur les carrés en face de chaque son. Pour que le clic fonctionne il faut vérifier que nous sommes bien dans le mode action.
- **Pd / Changer de carte son**. Pour changer de carte son, aller dans le menu média > paramètres audio. Pour tester le son : menu media > tester l'audio et le midi et cliquer sur 80 ou 60 au haut à gauche. 
- **Ordi / Gérer l'accès au réseau**. Il y a deux options : soit l'ordinateur est relié avec un câble réseau (ethernet-RJ45) à la Raspberry Pi, soit il est relié en Wi-Fi à un routeur ou un téléphone portable configuré en point d'accès mobile. La deuxième option permet de relier l'ordinateur à Internet, donc d'installer des choses sur l'ordinateur. L'icône en bas à droite sur le tableau de bord permet de basculer entre les deux options : filaire ("Connexion RPI Lithica") ou sans fils ("AndroidAP").
- **Ordi / Mettre à jour le patch**. 
	- débrancher le câble réseeu et se connecter en sans-fils à Internet.
	- sur le bureau, ouvrir le dossier personnel (navigateur de fichiers), puis le dossier "lithica-tech".
	- clic droit dans la fenêtre, un menu s'ouvre, choisir "ouvrir dans un terminal"
	- écrire dans le terminal : git pull. Cela va récupérer les dernières modifications du projet.


## Configuration

**Poste central**
- 1 ordinateur central (portable ou mini type gigabyte) avec Pure Data qui gère les entrées provenant de Raspberry et sorties (rendu sonore, lumineux, etc.). IP manuel: 192.168.0.100
- 1 carte son / amplificateur, etc.

**Dans le cristal**
- 1 Raspberry Pi (RPI) avec capteurs pour chaque face. Un patch Pure Data récupère les données des capteurs (série) et les envoie en OSC sur l'ordinateur central.
- 1 routeur distribue les câbles ethernet.

### Install Laptop
- git clone puredata (pd.0.51) + compilation
- dépendances : malinette-abs/map, puremapping/mean_n


### Install RPI
- git clone puredata (pd.0.51) + compilation + dépendances
- comport ...
- pas de moocow/bytes2any : remplacement avec fudiparse (pd0.51)
- pour jevois : pas besoin de "sudo apt purge modemmanager"

### Nano
- Pour nano clone : select atmega328 (old bootloader)


## TODO
- lumières : matériels, scéno, scénario
- scéno générale
- vibration : matériels, scéno
- gants
- body pix : rpi cam

