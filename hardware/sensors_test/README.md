# Sensors

Ressources :

- https://www.digikey.fr/fr/articles/fundamentals-distance-measurement-gesture-recognition-tof-sensors
- https://www.robotshop.com/eu/fr/comparatif-capteurs-distance-infrarouges-ultrasonique.html
- https://itp.nyu.edu/physcomp/distance-sensors-the-basics/
- https://www.sparkfun.com/distance_sensor_comparison_guide
- https://www.sparkfun.com/distance_sensing
- https://projetsdiy.fr/comparaison-hc-sr04-ultrasons-sharp-gp2y0a02yk0f-ir-vl53l0x-laser-quelle-solutions-choisir-mesure-distance-arduino-raspberry-pi/


## Synthèse


- OK. US URM37 : 2-800cm, sampling 100ms max, 60°

- 24GHz Microwave Radar Sensor: 50cm - 20m : ?FoV, sauts de valeurs : UART : cher, harsh environments
- ToF VLX : <= 1.2m. RAPIDE. ? distance mode, saute 0/8190 alors qu'il n'y a rien. Lumières soleil!! works best in dark conditions
- US URM07 : 20 - 750cm, FoV - 60°, rate 170ms, ok avec surface plate, ? corps ?, ? array ?, harsh environmental
- TOF Nooploop Sense Laser Range Sensor : <= 5m, trop lent, ? default range 2m ?, FoV 27°, near-infrared light from the sun's light, which affects the measurement of the module
- OpenTheremin : <= 30cm, pas directif, ?deuxième entrée : ?calibrations

## Jevois Camera NoIR lens

Ressources :

- jevois
- Computer vision for beginners part 4
- https://www.learnpythonwithrune.org/opencv-understand-and-implement-a-motion-tracker/
- https://www.learnpythonwithrune.org/opencv-and-python-simple-noise-tolerant-motion-detector/

Caractéristiques :
- Camera field of view: 65 degrees horizontal, F2.8, 4:3 sensor aspect ratio.

Retours :

- guvcview -ao none -f YUYV -x 640x360 -d /dev/video2 # On Raspberry Pi host, use '-a none -o none' instead of '-ao none'
- lumiere IR pour lentille NoIR. 
- Possibilité de lentilles basse luminosité. [JeVois 0.3MP Low-Light Sensor](https://www.jevoisinc.com/products/copy-of-jevois-0-3mp-low-light-sensor-with-standard-90deg-120deg-and-noir-lenses)
- Soft un peu instable
- Démarrer module ?
- Affiner module
- Envoyer juste les informations en série
- Ombres ! Luminosité !
- Bruit ventilateur ! page heatsink onto the GPU avec colle
- tutorials opencv_py_contour_features

Installation :

- sudo apt-get install guvcview && guvcview -ao none -f YUYV -x 640x360
- jevois-inventor (http://jevois.org/start/start.html)
- sudo apt purge modemmanager
- installation (optionnel ?) "getting JeVois developer software - the quick way" ! plusieurs Go
- éviter autogain et autoexposure
- sudo apt-get install screen. sudo screen /dev/ttyACM0 115200. CTRL-A ESC \ yes


Config :
- New Python Module. Editer fps 30 sans virgules....
- jevois-inventor : File > Preferences : Headless + startup module Lithica PyMotion


~/JEVOIS/config/initscript.cfg

```
# .../JEVOIS/config/initscript.cfg
setmapping2 YUYV 320 240 30.0 Lithica PyMotionv1
# Serial 4 pin to Arduino
# setpar serout Hard
# Serial Over USB
setpar serout USB
setcam autogain 0
setcam autoexp 1
streamon
```


Cam controls setcam <ctrl> <val> :
- brightness [int] min=-3 max=3 step=1 def=0 curr=0
- contrast [int] min=0 max=6 step=1 def=3 curr=3
- saturation [int] min=0 max=4 step=1 def=2 curr=2
- autowb [bool] default=1 curr=0
- dowb [int] min=0 max=1 step=1 def=0 curr=1
- redbal [int] min=0 max=255 step=1 def=128 curr=125
- bluebal [int] min=0 max=255 step=1 def=128 curr=151
- autogain [bool] default=1 curr=1
- gain [int] min=16 max=1023 step=1 def=16 curr=58
- hflip [bool] default=0 curr=0
- vflip [bool] default=0 curr=0
- powerfreq [menu] values 0:disabled 1:50hz 2:60hz curr=2
- sharpness [int] min=0 max=32 step=1 def=6 curr=6
- autoexp [menu] values 0:auto 1:manual curr=0
- absexp [int] min=1 max=1000 step=1 def=1000 curr=500
- presetwb [menu] values 0:manual 1:auto 2:incandescent 3:fluorescent 4:fluorescent_h 5:horizon 6:daylight 7:flash 8:cloudy 9:shade curr=1

http://jevois.org/doc/Lenses.html
An infrared-cut (IR-cut) filter blocks infrared light so that the sensor only receives visible light. This ideal for daytime use. The NoIR lens with no such filter is ideal for nighttime use. You would then need to provide an infrared light source (typically, an array of infrared LEDs), which can illuminate your scene using a wavelength of light that is invisible to the human eye. In this way, JeVois can still "see", thanks to the reflected IR light, even in what appears to humans to be complete darkness. Many security cameras use this feature to provide nighttime monitoring.


Standard lens: most versatile, no distortion, fixed focus of 40cm to infinity. Provides a relatively narrower field of view but this means that objects appear bigger and have more pixels on target, possibly making them easier to understand by JeVois (e.g., decoding of QR-codes or ArUco markers).
NoIR lens: no distortion, same field of view as the standard lens, adjustable focus, no IR-cut filter. Mostly useful for night-vision applications, using an infrared illuminator as a light source.

## HuskyLens
https://wiki.dfrobot.com/HUSKYLENS_V1.0_SKU_SEN0305_SEN0336

Gravity: HUSKYLENS - An Easy-to-use AI Machine Vision Sensor
Project Interactive Gesture Control

HuskyLens is an easy-to-use AI machine vision sensor. It can learn to detect objects, faces, lines, colors and tags just by clicking.Through the UART / I2C port, HuskyLens can connect to Arduino, Raspberry Pi, LattePanda, or micro:bit, and make your very creative projects without playing with complex algorithms

## RPI PI cam NoIR

Retours : 
- Pas de tests

Ressources :
- http://dronebotworkshop.com/pi-cameras/
- https://www.raspberrypi.org/documentation/usage/camera/installing.md
- https://www.raspberrypi.org/documentation/usage/camera/README.md

Installations :

- Enabling the camera. Menu Preferences > Raspberry Pi Configuration > Interfaces tab > enable camera option > reboot
- Or sudo raspi-config > Interfacing Options > Camera + Enter > Finish > Reboot
- raspistill (photo), raspivd and raspiyuv are command line tools for using the camera module.
- raspivid -o vid.h264 (capture 5 seconds)

- raspivid
- raspistill
- github motioneyeos
- sudo apt install -y gpac (mp4box utility)
- sudo apt install python-camera python3-camera
- pinoir v2 mode 4 fast capture with python+opencv
- rpi-opencv
- real-time motion detection in Raspberry Pi

## Theremin

- direction, velocity, ...
- instabilité ?
- interférences ?

### Touchboard
Ressources :
- gradual proximity sensing

Retours :

- petit 10cm
- 10 entrées !

Installation :

- soudure midi pins x 2
- téléchargement Touch Board Installer
- select board > touch board > midi
- Ouvrir Midi_proximity.ino : Electrode 0 proximity sensor > midi controller 102

### Open Theremin

Ressources:
- gaudi.ch opentheremin


Caractéristiques :
- very sensitive devices. need calibration before playing

Retours :

- ?? autocalibration ?? attendre 10min !!
- 40cm avec carré de peinture 12cm x 12cm.
- Marche pas bien avec juste une antenne

Installation :

- soudure
- téléchargement de ttymidi
- compilation ttymidi : add lpthread : gcc src/ttymidi.c -lpthread -o ttymidi -lasound
- téléchargement de OpenTheremin_V3_with_MIDI-master
- téléversement sur Arduino

### Andrey Smirnov

- pas de nouvelles

## IR

- très directif
- courte portée
- interférences : réflectivité de l'objet et l'intensité de la lumière ambiante.

## ToF VL53L0X

Ressources :

- https://www.dfrobot.com/product-1706.html
- https://wolles-elektronikkiste.de/en/vl53l0x-and-vl53l1x-tof-distance-sensors
- https://www.seeedstudio.com/blog/2020/01/08/what-is-a-time-of-flight-sensor-and-how-does-a-tof-sensor-work/

Caractéristiques :

- I2C
- 30-2000mm
- FoV 25°
- Sampling Time <= 30ms
- ToF technology does not critically depend on angle of obstacle.
- ToF Not affected by object characteristics: Size, dimensions, materials used, etc.

Retours:

- rapide
- précis
- choix entre HIGH_SPEED or HIGH_ACCURACY
- assez petite distance <= 1.2 (voir VL53L1_SetDistanceMode()). Default ? Long (default)Up to 4 m
- sauts 0 / 8190 sans mouvements
- interférences lumières soleil ? It works best in dark conditions.
- RAPIDE. Précis <= 1.2m (??? distance mode). Saute 0/8190 alors qu'il n'y a rien. Lumières soleil!!

The VL53L0X's 940nm VCSEL emitter (vertical cavity surface emitting laser) is totally invisible to the human eye, coupled with internal physical infrared filters, it enables longer ranging distance, higher immunity to ambient light and better robustness to cover-glass optical cross-talk.

bool setMeasurementTimingBudget(uint32_t budget_us)
Sets the measurement timing budget to the given value in microseconds. This is the time allowed for one range measurement; a longer timing budget allows for more accurate measurements. The default budget is about 33000 microseconds, or 33 ms; the minimum is 20 ms. The return value is a boolean indicating whether the requested budget was valid.

Timing budget and inter-measurement period
Status = VL53L1_SetMeasurementTimingBudgetMicroSeconds(&VL53L1Dev, 66000 ); sets the timing budget to 66 ms.
The minimum and maximum timing budgets are [20 ms, 1000 ms]

An alternative and preferred way to get the ranging status is to use the physical interrupt output: by default, GPIO1 is pulled down when new ranging data are ready.

Status : 0 VL53L1_RANGESTATUS_RANGE_VALID Ranging measurement is valid

Installation :

- lib DfRobot
- lib Polulu
- lib Adafruit

## TOF Nooploop Sense Laser Range Sensor

Ressources :

- https://wiki.dfrobot.com/TOF%20Sense%20Laser%20Ranging%20%20Sensor(5m)%20SKU:SEN0337
- https://www.waveshare.com/wiki/TOF_Laser_Range_Sensor

Caractéristiques:

- Short-range: 0.012m~2.16m / Mid-range: 0.012m~3.60m / Long-range: 0.01m~5.00m (?? default)
- FoV 15°- 27° (default) adjustable
- Wave Length 940nm
- Interférences : In the outdoor environment, there is near-infrared light from the sun's light, which affects the measurement of the module.
- UART

Retours :

- Ok mais sampling rate assez lent : 180 - 500ms

## URM07 : Ultrasonic Sensor

Ressources :

- https://wiki.dfrobot.com/URM07-UART_Ultrasonic_Sensor_SKU__SEN0153

Caractéristiques :

- harsh environmental
- 20 - 750cm
- 60°
- UART

Retours :

- ?! marche bien avec un cahier mais pas trop avec corps/vetements !!
- sampling rate ~ 170ms
- Sonar Sensor Array ?

## URM37 : Ultrasonic Sensor

Ressources :

- https://wiki.dfrobot.com/URM37_V5.0_Ultrasonic_Sensor_SKU_SEN0001_

Caractéristiques :

- UART, ANALOG
- 5-500cm to 2-800cm
- FoV 60°
- built-in temperature compensation circuit of the module is able to increase the accurary of the measurement.
- barrier surface reflection of the sound is affected by many factors (such as barrier shape, orientation and texture) the influence of ultrasonic distance measurement is therefore limited

Retours :

- Serial mode....
- Ok.

## US MB1010
- https://www.maxbotix.com/Arduino-Ultrasonic-Sensors-085/

## 24GHz Microwave Radar Sensor

Ressources :

- https://wiki.dfrobot.com/24GHz_Microwave_Radar_Sensor_SKU:%20SEN0306

Caractéristiques :

- harsh environments
- 50cm - 20m
- ? FoV ?
- UART

Retours :

- ? multiple targets
- sauts de valeurs

## US HC-SR04

US technology demande grande surface ?

Caractéristiques :
- 2-400cm
- FoV : 15° ou 30° ?

Ressources :
- ajouter résistances TRIG + ECHO (theremino.com)
- Object Tracking in 2D with Ultrasound sensor
- octosonar
- stationary radar lidar with Arduino (instrutables)
- http://tpil.projet.free.fr/TP_Arduino/07-Ultrason.html
- https://randomnerdtutorials.com/complete-guide-for-ultrasonic-sensor-hc-sr04/
- https://towardsdatascience.com/building-a-sonar-sensor-array-with-arduino-and-python-c5b4cf30b945
- https://platis.solutions/blog/2017/08/27/sonicdisc-360-ultrasonic-scanner/
- https://www.intorobotics.com/how-to-use-sensor_msgs-range-ros-for-multiple-sensors-with-rosserial/ + https://www.intorobotics.com/how-to-install-ros-melodic-rosserial-and-more-on-raspberry-pi-4-raspbian-buster/


## Array US HC-SR04

Ressources :
- http://olliesworkshops.blogspot.com/2016/02/ultrasonic-sensor-array.html
- http://olliesworkshops.blogspot.com/2016/07/ultrasonic-sensor-array-library.html
  Serial.begin(230400);       // Initialize Serial communication


Extra delay between channels is 60/62500 = 0.96 ms
~10ms

Config : Pin Change interrupt PCINT. The trigger signals could be connected to any digital output signals
- US 1 : 19 (trig), 53 (echo / PB0 / PCINT0)
- US 2 : 18 (trig), 52 (echo / PB1 / PCINT1)
- US 3 : 17 (trig), 51 (echo / PB2 / PCINT2)
- US 4 : 16 (trig), 50 (echo / PB3 / PCINT3)


Other :
- PB4 in pin 10 PCINT4
- PB5 in pin 11 PCINT5
- PB6 in pin 12 PCINT6
- PB7 in pin 13; PCINT7 should be left for LED output.


The low cost ultrasonic sensors without the guidance cones are not very reliable and can easily give occasional false readings. For that reason, your sensor processing should include some level filtering, such as an Exponential filter. Even with a heavy filtering, the sensor is fast enough for most mechanical controls.



## Thermal Array
MLX90641 16x12 plus disponibles
FoV 55x35°
FoV 110x75°7

55x35° ou  110x75°


Ressources :
- makersportal.com thermal camera mlx90640
- adafruit mlx90640
- modules python pypi.org mlx9064x-blob-detection

## Depth camera
- Kinect v2: 512 x 424px. FoV : 70°x60°
- Intel RealSense Depth Camera D455 (SDK linux). 60cm - 6m. FoV 86x57°. Fps 90. 1280x800px.

- Complexe, demande beaucoup de ressources
- Pas possible pour l'installation

## Flir Lepton
- trop cher
- sampling rate 9hz
- Fov 50°


## LIDAR
- pas adapté. FoV trop petit (2°!)


## Pixy2 Camera
- Pas testé
- petits objets, peut-être pas détection de personnes

## Potenta Vision Shield
- Pas testé
- Prise Ethernet

## Thermal camera AMG8833
- lib adafruit pour Arduino

Ressources :
- https://makersportal.com/blog/thermal-camera-analysis-with-raspberry-pi-amg8833
- https://cdn-learn.adafruit.com/downloads/pdf/adafruit-amg8833-8x8-thermal-camera-sensor.pdf

