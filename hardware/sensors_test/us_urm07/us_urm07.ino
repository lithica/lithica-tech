// URM07-UART Ultrasonic Sensor
// 20-750cm distance measurement, the received data is not verified
// VCC > D13
// GND > D12
// TX  > D11
// RX  > D10

#include <SoftwareSerial.h>

#define header_H    0x55 //Header
#define header_L    0xAA //Header
#define device_Addr 0x11 //Address
#define data_Length 0x00 //Data length
#define get_Dis_CMD 0x02 //Command: Read Distance
#define checksum    (header_H+header_L+device_Addr+data_Length+get_Dis_CMD) //Checksum

unsigned char i = 0;
unsigned int  distance = 0;
unsigned char Rx_DATA[8];
unsigned char CMD[6] = { header_H, header_L, device_Addr, data_Length, get_Dis_CMD, checksum }; // Distance command package

SoftwareSerial Serial1(11,10); //Serial1 URM07 Serial：RX > 10, TX > 11

void setup() {
  pinMode(9, OUTPUT);
  pinMode(8, OUTPUT);
  digitalWrite(9, HIGH);  // Ultrasonic VCC
  digitalWrite(8, LOW);   // Ultrasonic GND
  Serial.begin(38400);
  Serial1.begin(19200); // Serial1: Ultrasonic baudrate: 19200
}

void loop() {
  for(i=0;i<6;i++){
    Serial1.write(CMD[i]);
  }
  delay(100);  //Wait for the result
  
  i=0;
  while (Serial1.available()){  // Read the return data no data verification
    Rx_DATA[i++]=(Serial1.read());
  }
  distance=( (Rx_DATA[5] << 8 ) | Rx_DATA[6] ); // Read the distance value
  Serial.println(distance);
}
