# Open Theremin with MIDI arduino firware
# DEBUG ttymidi compilation : add lpthread : gcc src/ttymidi.c -lpthread -o ttymidi -lasound

# start ttymidi with Arduino UNO
./ttymidi -s /dev/ttyACM0 &

# start Pd ALSA compatible MIDI 
pd -open ./ttymidi.pd &                    

sleep 7 

# list available MIDI input clients
#aconnect -i                       

# list available MIDI output clients
#aconnect -o                       

# connect 128 (ttymidi) to Pd (129)
aconnect 128:0 129:0

# ...where 128 and 129 are the client 
# numbers for ttymidi and timidity,
# found with the commands above
