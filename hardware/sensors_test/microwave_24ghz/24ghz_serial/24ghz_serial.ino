/*
 * Test Radiowave 24 ghz - 1 output
 * 
 * Detection Distance：0.5~20m
 * 
 * Wiring : 
 * VCC/GND
 * green > pin 4
 * orange > pin 5
 * 
 */

#include <SoftwareSerial.h>

char col; // For storing the data read from serial port
unsigned char buffer_RTT[8] = {};
int YCTa = 0, YCTb = 0, YCT1 = 0;
SoftwareSerial mySerial(4, 5);

const int MAX = 350; // max 200cm

void setup() {
  mySerial.begin(57600); // Radiowave communication    
  Serial.begin(38400); // output
}

void loop() { 
  // Send data only when received data
  if (mySerial.read() == 0xff) {    
    // Read the incoming byte.
    for (int j = 0; j < 8; j++){
      col = mySerial.read();
      buffer_RTT[j] = (char)col;
      delay(2);        
     } 
          
     mySerial.flush();

     if(buffer_RTT[0] == 0xff){
      if(buffer_RTT[1] == 0xff){
        if(buffer_RTT[2] == 0xff){
          YCTa = buffer_RTT[3];      
          YCTb = buffer_RTT[4];
          YCT1 = (YCTa << 8) + YCTb;               
         }
       }
     }//Read the obstacle distance of maximum reflection intensity

     if (YCT1 < MAX) {
      Serial.println(YCT1); //Output the obstacle distance of maximum reflection intensity
     }
  } 
}
