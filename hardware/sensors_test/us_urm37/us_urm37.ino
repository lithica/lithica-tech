//
// # Working Mode: Serial  Mode.

#include <SoftwareSerial.h>
SoftwareSerial mySerial(10,11); // TX board, RX board

unsigned int Distance = 0;
uint8_t DistanceCmd[4] = {0x22, 0x00, 0x00, 0x22}; // distance measure command
uint8_t DistanceData[4];

void setup()
{
  Serial.begin(38400);
  mySerial.begin(9600);
  delay(500);
}

void loop()
{
  Serial1Cmd();
  delay(100);
}


void Serial1Cmd()
{
  int i;
  for (i = 0; i < 4; i++) {
    mySerial.write(DistanceCmd[i]);
  }
  while (mySerial.available() > 0)  // if received data
  {
    for (i = 0; i < 4; i++) {
      DistanceData[i] = mySerial.read();
    }
    Distance = DistanceData[1] << 8;
    Distance = Distance + DistanceData[2];
    Serial.println(Distance, DEC);
  }
}
