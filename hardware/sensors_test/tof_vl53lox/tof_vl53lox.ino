/* This example shows how to use continuous mode to take
range measurements with the VL53L0X. It is based on
vl53l0x_ContinuousRanging_Example.c from the VL53L0X API.

The range readings are in units of mm. */

#include <Wire.h>
#include <VL53L0X.h>

VL53L0X sensor;

static int tolerance = 2;
static int minRange = 50; // 5cm min
static int maxRange = 2000; // 2m max
int distance, lastDistance = 0;

void setup()
{
  Serial.begin(38400);
  Wire.begin();

  sensor.setTimeout(500);
  sensor.setAddress(0x50);
  if (!sensor.init())
  {
    //Serial.println("Failed to detect and initialize sensor!");
    while (1) {}
  }

  // Start continuous back-to-back mode (take readings as
  // fast as possible).  To use continuous timed mode
  // instead, provide a desired inter-measurement period in
  // ms (e.g. sensor.startContinuous(100)).
  sensor.startContinuous();
}

void loop()
{
  distance = sensor.readRangeContinuousMillimeters();
  if (distance > minRange && distance < maxRange ) {
    if (distance < lastDistance-tolerance || distance > lastDistance+tolerance) { 
      Serial.println(distance);
      lastDistance = distance;
      delay(5);
    }
  }
}
