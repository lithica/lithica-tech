/*******************************************************************************
 * 
 * Touch board midi
 * Electrode 0 proximity sensor > midi controller 102
 * 
 * Source : mix of touchboard examples
 * 
 * 
*******************************************************************************/

// compiler error handling
#include "Compiler_Errors.h"

// touch includes
#include <MPR121.h>
#include <MPR121_Datastream.h>
#include <Wire.h>

// MIDI includes
#include "Midi_object.h"

// mapping and filter definitions
#define LOW_DIFF 0
#define HIGH_DIFF 50
#define filterWeight 0.3f // 0.0f to 1.0f - higher value = more smoothing
float lastProx = 0;

const boolean DEBUG = false; // send serial debug
const boolean COMM_MIDI = true; // send midi

// touch constants
const uint32_t BAUD_RATE = 38400;//115200;
const uint8_t MPR121_ADDR = 0x5C;
const uint8_t MPR121_INT = 4;

// MIDI instantiation
MIDIEvent e;

// MIDI variable
midi_object_type MIDIobjects[12];  // create an array of MIDI objects to use (one for each electrode)

// MIDI behaviour constants
const uint8_t CHANNEL = 1;  // edit to change the channel number

void setup() {
  if (DEBUG) Serial.begin(BAUD_RATE);
  pinMode(LED_BUILTIN, OUTPUT);
  setupMPR121(); 
  if (COMM_MIDI) setupMidi();
}

void loop() {
  MPR121.updateAll();
  getInput();
  if (COMM_MIDI) MIDIUSB.flush();  // flush USB buffer to ensure all notes are sent
  delay(10);  // 10ms delay to give the USB MIDI target time to catch up
}


void getInput(){
  // read the difference between the measured baseline and the measured continuous data
  int reading = MPR121.getBaselineData(0)-MPR121.getFilteredData(0);

  // constrain the reading between our low and high mapping values
  unsigned int prox = constrain(reading, LOW_DIFF, HIGH_DIFF);
  
  // implement a simple (IIR lowpass) smoothing filter
  lastProx = (filterWeight*lastProx) + ((1-filterWeight)*(float)prox);

  // map the LOW_DIFF..HIGH_DIFF range to 0..127 
  uint8_t thisOutput = (uint8_t)map(lastProx,LOW_DIFF,HIGH_DIFF,0,127);

  // debug
  if (DEBUG) {
    Serial.print(MPR121.getBaselineData(0)); 
    Serial.print(":");
    Serial.print(MPR121.getFilteredData(0)); 
    Serial.print(":");
    Serial.println(thisOutput); 
  }

  // send midi
  if (COMM_MIDI) {
    // output the correctly mapped value from the input
    //e.m3 = (unsigned char)constrain(map(MPR121.getFilteredData(i), MIDIobjects[i].inputMin, MIDIobjects[i].inputMax, MIDIobjects[i].outputMin, MIDIobjects[i].outputMax), 0, 127);
    e.m3 = thisOutput;
     if (e.m3 != MIDIobjects[0].lastOutput) {  // only output a new controller value if it has changed since last time
        MIDIobjects[0].lastOutput = e.m3;
        e.type = 0x08;
        e.m1 = uint8_t(0xB << 4 | CHANNEL);
        e.m2 = MIDIobjects[0].controllerNumber;
        MIDIUSB.write(e);
      }
  }
}

void setupMidi() {

    // set up electrode 0 as a proxmity mapped controller attached to controller 102
    MIDIobjects[0].type = MIDI_CONTROL;
    MIDIobjects[0].controllerNumber = 102;  // 102..119 are undefined in the MIDI specification
    MIDIobjects[0].inputMin = 520;   // note than inputMin is greater than inputMax here
                                     // this means that the closer your hand is to the sensor
                                     // the higher the output value will be
                                     // to reverse the mapping, make inputMax greater than inputMin
    MIDIobjects[0].inputMax = 480;   // the further apart the inputMin and inputMax are from each other
                                     // the larger of a range the sensor will work over
    MIDIobjects[0].outputMin = 0;    // minimum output to controller - smallest valid value is 0
    MIDIobjects[0].outputMax = 127;  // maximum output to controller - largest valid value is 127


    //....
}


void setupMPR121(){
if (!MPR121.begin(MPR121_ADDR)) {
    Serial.println("error setting up MPR121");
    switch (MPR121.getError()) {
      case NO_ERROR:
        Serial.println("no error");
        break;
      case ADDRESS_UNKNOWN:
        Serial.println("incorrect address");
        break;
      case READBACK_FAIL:
        Serial.println("readback failure");
        break;
      case OVERCURRENT_FLAG:
        Serial.println("overcurrent on REXT pin");
        break;
      case OUT_OF_RANGE:
        Serial.println("electrode out of range");
        break;
      case NOT_INITED:
        Serial.println("not initialised");
        break;
      default:
        Serial.println("unknown error");
        break;
    }
    while (1);
  }

   MPR121.setInterruptPin(MPR121_INT);

  // slow down some of the MPR121 baseline filtering to avoid filtering out slow hand movements
  MPR121.setRegister(MPR121_NHDF, 0x01); //noise half delta (falling)
  MPR121.setRegister(MPR121_FDLF, 0x3F); //filter delay limit (falling)  

  MPR121.updateAll(); 
}
