import libjevois as jevois
import cv2
import numpy as np

## Detect Motions
#
# Add some description of your module here.
#
# @author Jerome Abel
# 
# @videomapping YUYV 320 240 30 YUYV 320 240 30 Lithica PyMotionv1
# @email contact@jeromeabel.net
# @address 123 first street, Los Angeles CA 90012, USA
# @copyright Copyright (C) 2018 by Jerome Abel
# @mainurl http://jeromeabel.net
# @supporturl http://jeromeabel.net
# @otherurl http://jeromeabel.net
# @license GPL v3
# @distribution Unrestricted
# @restrictions None
# @ingroup modules
class PyMotionv1:
    # ###################################################################################################
    ## Constructor
    def __init__(self):
        self.backSub = cv2.createBackgroundSubtractorMOG2()
        self.kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (3, 3))
        
    # ###################################################################################################
    ## Process function with no USB output
    def processNoUSB(self, inframe):
        img_gray = inframe.getCvGRAY()
        img_blur = cv2.bilateralFilter(img_gray, d = 7, sigmaSpace = 75, sigmaColor =75)
        frameDiff = self.backSub.apply(img_blur)
        frameDiff = cv2.morphologyEx(frameDiff, cv2.MORPH_OPEN, self.kernel)
        thresh = cv2.threshold(frameDiff, 50, 255, cv2.THRESH_BINARY)[1]
        cv2.dilate(thresh, None, iterations=4)
        cv2.erode(thresh, None, iterations=2)
        contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        contours = sorted(contours, key = cv2.contourArea, reverse = True)
        
        #Get biggest area
        #c0 = max(contours, key = cv2.contourArea)
        #area0 = cv2.contourArea(c0)
        #cx0 = int(M['m10'] / M['m00'])
        #cy0 = int(M['m01'] / M['m00'])
        #jevois.sendSerial("{} {} {} {}".format(0, area0, cx0, cy0))
        
        # All areas
        for index, c in enumerate(contours):
            area = cv2.contourArea(c)
            if area > 1000:
                M = cv2.moments(c)
                # The centroid point
                cx = int(M['m10'] / M['m00'])
                cy = int(M['m01'] / M['m00'])
                jevois.sendSerial("{} {} {} {}".format(index, area, cx, cy))
        
    # ###################################################################################################
    ## Process function with USB output
    def process(self, inframe, outframe):
        img_gray = inframe.getCvGRAY()
        img_blur = cv2.bilateralFilter(img_gray, d = 7, sigmaSpace = 75, sigmaColor =75)
        #img_blur = cv2.GaussianBlur(img_gray, (21, 21), 0) #21,21 11,11
        #a = img_gray.max()
        # a/2+60, a
        frameDiff = self.backSub.apply(img_blur)
        frameDiff = cv2.morphologyEx(frameDiff, cv2.MORPH_OPEN, self.kernel)
        thresh = cv2.threshold(frameDiff, 50, 255, cv2.THRESH_BINARY)[1]
        cv2.dilate(thresh, None, iterations=4)
        cv2.erode(thresh, None, iterations=2)
        contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        contours = sorted(contours, key = cv2.contourArea, reverse = True)
        #img_copy = img_gray.copy()
        #final = cv2.drawContours(img_copy, contours, contourIdx = -1, color = (255, 0, 0), thickness = 2)
        for index, c in enumerate(contours):
            area = cv2.contourArea(c)
            if area > 1000:
                M = cv2.moments(c)
                # The centroid point
                cx = int(M['m10'] / M['m00'])
                cy = int(M['m01'] / M['m00'])
                #c = max(contours, key = cv2.contourArea)
                x,y,w,h = cv2.boundingRect(c)
                cv2.rectangle(thresh,(x,y),(x+w,y+h),(255, 255, 255), 5)
        outframe.sendCv(thresh)
        
    # ###################################################################################################
    ## Parse a serial command forwarded to us by the JeVois Engine, return a string
    def parseSerial(self, str):
        jevois.LINFO("parseserial received command [{}]".format(str))
        if str == "hello":
            return self.hello()
        return "ERR Unsupported command"
    
    # ###################################################################################################
    ## Return a string that describes the custom commands we support, for the JeVois help message
    def supportedCommands(self):
        # use \n seperator if your module supports several commands
        return "hello - print hello using python"

    # ###################################################################################################
    ## Internal method that gets invoked as a custom command
    def hello(self):
        return "Hello from python!"
        
