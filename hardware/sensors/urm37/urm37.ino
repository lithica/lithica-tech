// URM37 sensor
// Nano clone : ATMEGA328 (old bootloader)
// Working Mode: Serial  Mode.

#include <SoftwareSerial.h>
SoftwareSerial mySerial(11, 10);

unsigned int distance = 0;
uint8_t distanceCmd[4] = {0x22, 0x00, 0x00, 0x22}; // distance measure command
uint8_t distanceData[4];

void setup()
{
  Serial.begin(9600);
  mySerial.begin(9600);
  delay(500);
}

void loop()
{
  getDistance();
  delay(80);
}

void getDistance()
{
  int i;
  
  // Send command
  for (i = 0; i < 4; i++) {
    mySerial.write(distanceCmd[i]);
  }
  
  // if received data
  while (mySerial.available() > 0)  
  {
    for (i = 0; i < 4; i++) {
      distanceData[i] = mySerial.read();
    }
    
    distance = distanceData[1] << 8;
    distance = distance + distanceData[2];
    //distance = map(distance, 30, 300, 0, 255);
    //distance = constrain(distance, 0, 255);
    //Serial.write(distance);
    Serial.println(distance, DEC);
  }
  
}